Social Auth Modal
=================

INTRODUCTION
------------

Social Auth Modal allows users to authenticate with social networks without
leaving the current page.
The authentication page will be opened in a new modal window.
After completing the authentication, the modal window will be automatically
closed and the current page will be reloaded for applying authenticated session.

This module is based on [Social Auth](https://www.drupal.org/project/social_auth)
and [Social API](https://www.drupal.org/project/social_api) projects.

It adds to the site:

* A Social Auth Modal Login block.
* A settings form at `/admin/config/social-api/social-auth/modal-settings`.
* A new technical URL for finishing authentication: `/user/login/complete`.

REQUIREMENTS
------------

This module requires the following modules:

* [Social Auth](https://drupal.org/project/social_auth)
* [Social API](https://drupal.org/project/social_api)

INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
See [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for more details.


CONFIGURATION
-------------

1. Configure the Social Auth module and social network integration modules
   according to their documentation.
2. Navigate to Structure > Block Layout and place
   a "Social Auth Modal login" block somewhere on the site.

That's it!

**Note:** "Post login path" and "Redirect new users to Drupal user form"
settings of the Social Auth module won't be respected because of
the primary purpose of the Social Auth Modal module — keeping users on
the same page after authentication.
