<?php

namespace Drupal\social_auth_modal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Social Auth Modal settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_auth_modal_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['social_auth_modal.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('social_auth_modal.settings');
    $form['modal_width'] = [
      '#type' => 'number',
      '#min' => 320,
      '#max' => 2560,
      '#field_suffix' => ' ' . $this->t('pixels'),
      '#title' => $this->t('Modal width'),
      '#default_value' => $config->get('modal_width'),
      '#required' => TRUE,
    ];
    $form['modal_height'] = [
      '#type' => 'number',
      '#min' => 240,
      '#max' => 1440,
      '#field_suffix' => ' ' . $this->t('pixels'),
      '#title' => $this->t('Modal height'),
      '#default_value' => $config->get('modal_height'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('social_auth_modal.settings')
      ->set('modal_width', $form_state->getValue('modal_width'))
      ->set('modal_height', $form_state->getValue('modal_height'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
