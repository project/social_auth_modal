<?php

namespace Drupal\social_auth_modal\EventSubscriber;

use Drupal\Core\Render\PageDisplayVariantSelectionEvent;
use Drupal\Core\Render\RenderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides page display variant subscriber.
 */
final class PageDisplayVariantSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      RenderEvents::SELECT_PAGE_DISPLAY_VARIANT => [
        'onSelectPageDisplayVariant',
        -100,
      ],
    ];
  }

  /**
   * Reacts on page display variant selection.
   *
   * @param \Drupal\Core\Render\PageDisplayVariantSelectionEvent $event
   *   The page display variant select event.
   */
  public function onSelectPageDisplayVariant(PageDisplayVariantSelectionEvent $event): void {
    $route_match = $event->getRouteMatch();
    if ($route_match->getRouteName() != 'social_auth_modal.complete') {
      return;
    }

    // Using simple page variant for complete authentication page.
    $event->setPluginId('simple_page');
  }

}
