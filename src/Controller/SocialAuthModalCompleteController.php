<?php

namespace Drupal\social_auth_modal\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns response for "social_auth_modal.complete" route.
 */
class SocialAuthModalCompleteController extends ControllerBase {

  /**
   * The Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->requestStack = $container->get('request_stack');

    return $instance;
  }

  /**
   * Builds the response.
   */
  public function build(): array {
    // Using "not found" instead of access checking to camouflage URL.
    if ($this->currentUser()->isAnonymous()) {
      throw new NotFoundHttpException();
    }

    // Using "check_logged_in" GET parameter for checking that user was
    // just authenticated, for preventing visits by direct URL.
    if (!$this->requestStack->getCurrentRequest()->query->has('check_logged_in')) {
      throw new NotFoundHttpException();
    }

    // Main page functionality handled by JS.
    $build['#attached']['library'][] = 'social_auth_modal/modal_close';

    // The window should be closed by JS automatically, and the content of
    // the page is not intended to be displayed ever.
    // But in case it will not happen for some reason,
    // it can be closed manually as well.
    $build['text'] = [
      '#type' => 'item',
      '#markup' => $this->t('Please push the button below to close this window and get back to the site.'),
    ];

    $build['close_button'] = [
      '#type' => 'link',
      '#title' => $this->t('Close this window'),
      '#url' => Url::fromRoute('<button>'),
      '#attributes' => [
        'class' => ['button', 'button--primary', 'social-auth-modal__close'],
      ],
    ];

    return $build;
  }

}
