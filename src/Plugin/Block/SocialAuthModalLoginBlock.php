<?php

namespace Drupal\social_auth_modal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\social_api\Plugin\NetworkManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Social Auth Block for Modal Login.
 *
 * @Block(
 *   id = "social_auth_login_modal_block",
 *   admin_label = @Translation("Social Auth Modal Login"),
 * )
 */
class SocialAuthModalLoginBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The network manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  private NetworkManager $networkManager;

  /**
   * Immutable configuration for social_auth.settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $socialAuthConfig;

  /**
   * Immutable configuration for social_auth_modal.settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $socialAuthModalConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->socialAuthConfig = $container->get('config.factory')
      ->get('social_auth.settings');
    $instance->socialAuthModalConfig = $container->get('config.factory')
      ->get('social_auth_modal.settings');
    $instance->networkManager = $container->get('plugin.network.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    // Getting networks code adopted from original 'SocialAuthLoginBlock'.
    // See 'social_auth/src/Plugin/Block/SocialAuthLoginBlock.php'.
    $networks = $this->networkManager->getDefinitions();
    $social_networks = $this->socialAuthConfig->get('auth');
    $destination = Url::fromRoute('social_auth_modal.complete')->toString();
    $links = [];
    foreach ($social_networks as $id => &$social_network) {
      // Preparing raw data for passing to template.
      // Using the same structure as in original block for consistency.
      $social_network['name'] = NULL;
      if (isset($networks[$id]) && isset($networks[$id]['social_network'])) {
        $social_network['name'] = $networks[$id]['social_network'];
      }

      // Preparing data for block content.
      $options = [
        'attributes' => [
          'class' => ['social-auth', 'auth-link', 'social-auth-modal__link'],
        ],
      ];

      $url = Url::fromRoute($social_network['route'], ['destination' => $destination], $options);
      // Adding URL to raw data.
      $social_network['url'] = $url;

      $text = $social_network['name'] ?? $this->t('Untitled service');
      $links[$id] = Link::fromTextAndUrl($text, $url)->toRenderable();
    }

    $modal_config = $this->socialAuthModalConfig;

    return [
      '#attached' => [
        'library' => [
          'social_auth_modal/modal_open',
        ],
        'drupalSettings' => [
          'socialAuthModal' => [
            'width' => $modal_config->get('modal_width'),
            'height' => $modal_config->get('modal_height'),
          ],
        ],
      ],
      // Block content.
      'links' => [
        '#theme' => 'item_list',
        '#items' => $links,
        '#title' => $this->t('Authenticate with'),
      ],
      // Raw data.
      '#social_networks' => $social_networks,
      '#destination' => $destination,
    ];
  }

}
