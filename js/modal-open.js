// Variable for storing modal window object globaly.
// It will be needed for overriding /performing additional
// actions on modal window closing.
//
// var SocialAuthModal;

(function ($, Drupal, drupalSettings, once) {
  'use strict';

  Drupal.behaviors.socialAuthModalOpen = {
    attach: function(context, settings) {

      once('modalOpen', '.social-auth-modal__link', context).forEach(function (element) {
        $(element).click(function (e) {
          e.stopPropagation();
          e.preventDefault();

          var href = $(this).attr('href');
          var width = drupalSettings.socialAuthModal.width ?? 560;
          var height = drupalSettings.socialAuthModal.width ?? 720;

          popupCenter(href, 'User authentication', width, height);

          // In case of overriding/performing additional actions on modal window closing,
          // assign modal window object to variable instead of just calling the function.
          //
          // SocialAuthModal = popupCenter(href, 'User authentication', width, height);
        });
      });

    }
  };
}) (jQuery, Drupal, drupalSettings, once);

// Opens a modal window at the center of the page.
// Based on http://www.xtf.dk/2011/08/center-new-popup-window-even-on.html
function popupCenter(url, title, w, h) {
  // Fixes dual-screen position                         Most browsers       Firefox.
  var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
  var dualScreenTop  = window.screenTop  != undefined ? window.screenTop  : window.screenY;

  width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
  height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

  var left = ((width / 2) - (w / 2)) + dualScreenLeft;
  var top = ((height / 2) - (h / 2)) + dualScreenTop;
  var newWindow = window.open(url, title, 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

  // Puts focus on the newWindow.
  if (window.focus) {
    newWindow.focus();
  }

  return newWindow;
};

// An example of the function which should be called from modal window
// for overriding/performing additional actions.
//
// function SocialAuthModalClose() {
//   if (SocialAuthModal) {
//     SocialAuthModal.close();
//   }
//   window.location.reload();
// };
