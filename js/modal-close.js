(function ($, Drupal, once) {
  'use strict';

  Drupal.behaviors.socialAuthModalClose = {
    attach: function(context, settings) {

      once('modalAutoClose', 'html').forEach(function (element) {
        closePopup();
      });

      once('modalCloseButton', '.social-auth-modal__close').forEach(function (element) {
        $(element).click(function () {
          closePopup();
        });
      });

    }
  };
}) (jQuery, Drupal, once);

function closePopup() {
  if (window.opener) {
    window.opener.location.reload();
  }
  window.close();

  // To override/perform additional actions on modal window closing,
  // it is possible to call the function defined in the opener window
  // instead of/in addition to actions in this function.
  //
  // window.opener.SocialAuthModalClose();
};
